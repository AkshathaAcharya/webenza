import React from "react";
import {
  Route,
  BrowserRouter as Router,
  Switch,
  Redirect
} from "react-router-dom";
import { connect } from "react-redux";
import Logout from "../dashboard/dashboard";
import Main from "../main/main";
import Friends from "../friends/friends";
import Settings from "../settings/setting";
import Information from "../information/info";
class App extends React.Component {
  componentDidMount() {}
  render() {
    return (
      <div>
        <div>
          <Router>
            <Switch>
              <div>
                <Route exact path="/dashboard" component={Logout} />
                <Route exact path="/main" component={Main} />
                <Route exact path="/friends" component={Friends} />
                <Route exact path="/setting" component={Settings} />
                <Route exact path="/info" component={Information} />
                <Redirect from="/*" to="/main" component={Main} />
              </div>
              )}
            </Switch>
          </Router>
        </div>
      </div>
    );
  }
}
const stateToProps = state => {
  return {};
};
export default connect(stateToProps)(App);
