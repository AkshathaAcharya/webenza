import React from "react";
import { connect } from "react-redux";
import "antd/dist/antd.css";
import "../../assets/stylesheet/styles.css";
import "../main/main.css";
import { Link } from "react-router-dom";
import { Layout, Menu } from "antd";
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  HomeOutlined,
  DashboardOutlined,
  UsergroupAddOutlined,
  SettingFilled,
  InfoCircleOutlined
} from "@ant-design/icons";
import BackImage from "../../assets/main.png";
const { Header, Sider, Content } = Layout;

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: false
    };
  }
  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  };
  render() {
    return (
      <div>
        <Layout className="mainClass">
          <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
            <div className="logo" />
            <Menu
              theme="dark"
              mode="inline"
              className="sponsor_button"
              defaultSelectedKeys={["1"]}
              defaultOpenKeys={["sub1"]}
            >
              <Menu.Item className="increaseSize">
                <b>React Project</b>
              </Menu.Item>
              <Menu.Item>
                <b>Navigation</b>
              </Menu.Item>
              <Menu.Item key="1" icon={<HomeOutlined />}>
                Homepage
              </Menu.Item>
              <Menu.Item key="2" icon={<DashboardOutlined />}>
                <Link to={{ pathname: "/dashboard" }}>Dashboard </Link>
              </Menu.Item>

              <Menu.Item>
                <b>Another menu</b>
              </Menu.Item>
              <Menu.Item key="3" icon={<UsergroupAddOutlined />}>
                <Link to={{ pathname: "/friends" }}>Friends </Link>
              </Menu.Item>
              <Menu.Item key="4" icon={<SettingFilled />}>
                <Link to={{ pathname: "/setting" }}>Settings </Link>
              </Menu.Item>
              <Menu.Item key="5" icon={<InfoCircleOutlined />}>
                <Link to={{ pathname: "/info" }}>Information </Link>
              </Menu.Item>
            </Menu>
          </Sider>
          <Layout className="site-layout">
            <Header className="site-layout-background">
              {React.createElement(
                this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: this.toggle
                }
              )}
              <span className="heading">OFFICE PROJECT HOMEPAGE</span>
            </Header>
            <Content
              className="site-layout-background"
              style={{
                margin: "24px 16px",
                padding: 24,
                minHeight: 280
              }}
            >
              <img src={BackImage} width="100%" height="500px" alt="MAIN" />
            </Content>
          </Layout>
        </Layout>
      </div>
    );
  }
}
const dispachToProps = dispatch => {
  return {};
};

const stateToProps = state => {
  return {};
};

export default connect(stateToProps, dispachToProps)(Main);
